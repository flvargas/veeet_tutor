$( document ).ready(function() {

    var isMobile = function() {
        return $(window).width() < 768;
    }
    
    /*** Dispara animações através do scroll ***/
    var $window       = $(window),
    win_height_padded = $window.height() * 1.1;    
 
    $window.on('scroll', revealOnScroll);

    function revealOnScroll() {        
        if ( isMobile() ) {
            $(".revealOnScrollMobile").addClass("revealOnScroll"); // Animate elements only in Mobile
        }
        var scrolled = $window.scrollTop();

        $(".revealOnScroll:not(.animate__animated)").each(function () {
          var $this     = $(this),
              offsetTop = $this.offset().top;

          if (scrolled + win_height_padded > offsetTop) {
            if ($this.data('timeout')) {
              window.setTimeout(function(){
                animation = $this.data('animation');
                if ($this.data('animation-mobile')) { animation = $this.data('animation-mobile'); };
                $this.addClass('animate__animated animate__' + animation );
                
              }, parseInt($this.data('timeout'),10));
            } else {
                animation = $this.data('animation');
                if ($this.data('animation-mobile')) { animation = $this.data('animation-mobile'); };
                $this.addClass('animate__animated animate__' + animation );
            }
          }
        });

        // Reseta a anição no scroll UP
        $(".revealOnScroll.animate__animated").each(function (index) {
            var $this = $(this),
            offsetTop = $this.offset().top;
            if (scrolled + win_height_padded < offsetTop) {
                animation = $this.data('animation');
                if ($this.data('animation-mobile')) { animation = $this.data('animation-mobile'); };
                $(this).removeClass('animate__animated animate__' + animation);
            }
        });

        /*** Controla ativações dos botões do menu através do scroll ***/
        
        $('section').each(function (index) {
            var $this = $(this),           
            offsetTop = $this.offset().top - 230,
            offsetHeight = $this.outerHeight() - 80;

            $(".btn-" + $this.attr('id')).removeClass('active');

            if (scrolled > offsetTop && scrolled < offsetHeight+offsetTop) {
                $(".btn-" + $this.attr('id')).addClass('active');
            }           
        });        
    }

    /*** Slider Valores ***/
    var 
    zIndex = 1000,
    totalValores = 9,
    currentValor = 1,
    nextTextTransition = "animate__fadeInLeft",
    nextImgTransition = "animate__slideInLeft",
    prevTextTransition = "animate__fadeInRight",
    prevImgTransition = "animate__slideInRight";

    $(".slider-valores-next").click(function(e) {
        $(".valores-image.revealOnScroll, .valor.revealOnScroll").removeClass("animate__animated");
        $(".valores-image.revealOnScroll, .valor.revealOnScroll").css({"opacity":1});
        e.preventDefault();       
        zIndex++;
        currentValorTMP = currentValor;
        
        $("#valor_" + currentValor).hide();
        setTimeout(function(){
            $("#valor_image_" + currentValorTMP).hide();
        }, 800);
        
        if (currentValor == totalValores) {currentValor = 1;}
        else {currentValor++;}        

        $("#valor_image_" + currentValor).css({'z-index':zIndex});
        $(".valor p").removeClass( [nextTextTransition, prevTextTransition]);
        $(".valor-image").removeClass( [nextImgTransition, prevImgTransition]);
        $("#valor_" + currentValor + " p").addClass(nextTextTransition);
        $("#valor_image_" + currentValor).addClass(nextImgTransition);

        $("#valor_" + currentValor +", #valor_image_" + currentValor).show();
    });
    $(".slider-valores-previous").click(function(e) {
        $(".valores-image.revealOnScroll, .valor.revealOnScroll").removeClass("animate__animated");
        $(".valores-image.revealOnScroll, .valor.revealOnScroll").css({"opacity":1});
        e.preventDefault();
        zIndex++;
        currentValorTMP = currentValor;

        $("#valor_" + currentValor).hide();
        setTimeout(function(){
            $("#valor_image_" + currentValorTMP).show();
        }, 800);

        if (currentValor == 1) {currentValor = totalValores;}
        else {currentValor--;}

        $("#valor_image_" + currentValor).css({'z-index':zIndex});
        $(".valor p").removeClass( [nextTextTransition, prevTextTransition]);
        $(".valor-image").removeClass( [nextImgTransition, prevImgTransition]);
        $("#valor_" + currentValor + " p").addClass(prevTextTransition);
        $("#valor_image_" + currentValor).addClass(prevImgTransition);

        $("#valor_" + currentValor +", #valor_image_" + currentValor).show();
    });
    /*** END Slider Valores ***/

    /*** CUSTOM SELECT FORM ***/
    var x, i, j, l, ll, selElmnt, a, b, c;
    var classDark = "";
    /* Look for any elements with the class "custom-select": */
    x = document.getElementsByClassName("customSelect");
    l = x.length;
    for (i = 0; i < l; i++) {
      selElmnt = x[i].getElementsByTagName("select")[0];
      ll = selElmnt.length;
      /* For each element, create a new DIV that will act as the selected item: */
      a = document.createElement("DIV");
      a.setAttribute("class", "select-selected");
      a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
      x[i].appendChild(a);
      /* For each element, create a new DIV that will contain the option list: */
      b = document.createElement("DIV");
      b.setAttribute("class", "select-items select-hide");
      for (j = 1; j < ll; j++) {
        /* For each option in the original select element,
        create a new DIV that will act as an option item: */
        c = document.createElement("DIV");
        c.innerHTML = selElmnt.options[j].innerHTML;
        c.addEventListener("click", function(e) {
            /* When an item is clicked, update the original select box,
            and the selected item: */
            var y, i, k, s, h, sl, yl;
            s = this.parentNode.parentNode.getElementsByTagName("select")[0];
            sl = s.length;
            h = this.parentNode.previousSibling;
            for (i = 0; i < sl; i++) {
              if (s.options[i].innerHTML == this.innerHTML) {
                s.selectedIndex = i;
                h.innerHTML = this.innerHTML;
                y = this.parentNode.getElementsByClassName("same-as-selected");
                yl = y.length;
                for (k = 0; k < yl; k++) {
                  y[k].removeAttribute("class");
                }
                this.setAttribute("class", "same-as-selected");
                break;
              }
            }
            h.click();
        });
        b.appendChild(c);
      }
      x[i].appendChild(b);
      a.addEventListener("click", function(e) {
        /* When the select box is clicked, close any other select boxes,
        and open/close the current select box: */
        e.stopPropagation();
        closeAllSelect(this);
        this.nextSibling.classList.toggle("select-hide");
        this.classList.toggle("select-arrow-active");
      });
    }

    function closeAllSelect(elmnt) {
      /* A function that will close all select boxes in the document,
      except the current select box: */
      var x, y, i, xl, yl, arrNo = [];
      x = document.getElementsByClassName("select-items");
      y = document.getElementsByClassName("select-selected");

      xl = x.length;
      yl = y.length;
      for (i = 0; i < yl; i++) {
        if (elmnt == y[i]) {
          arrNo.push(i)
        } else {
          y[i].classList.remove("select-arrow-active");
        }
      }
      for (i = 0; i < xl; i++) {
        if (arrNo.indexOf(i)) {
          x[i].classList.add("select-hide");
        }
      }
      
      $(".select-selected").addClass('dark');
    }

    /* If the user clicks anywhere outside the select box,
    then close all select boxes: */
    document.addEventListener("click", closeAllSelect);

    /*** END CUSTOM SELECT ***/

    /*** Carousels ***/

    /* Carousel parceiros desktop */
     
    if ( $(window).width() < 1024 ) { visible = 3; }
    else { visible = 4; }
    $('div.carousel').jCarouselLite({
        scroll: 1,
        visible: visible,
        swipe: false,
        autoWidth: true,
        responsive: true,
        btnNext: '.next',
        btnPrev: '.prev',
        easing: 'easieEaseInOut',
        speed: 500
    });

    /* Carousel depoimentos mobile */
    $('div.carouselMobile').jCarouselLite({
        scroll: 1,
        visible: 1,
        swipe: false,
        autoWidth: true,
        responsive: true,
        btnNext: '.nextMobile',
        btnPrev: '.prevMobile',
        easing: 'easieEaseInOut',
        speed: 500
    });
    
    /*** END Carousels ***/

    /* Parceiros Mostrar Depoimento */

     $('.thumb a').click(function(e) {
        e.preventDefault();
        $('.thumb a').removeClass('selected');
        $(this).addClass('selected');

        testimony = $(this).parent().find('testimony').html();
        partnerName = $(this).parent().find('.partner-name').html();
        partnerPosition = $(this).parent().find('.partner-position').html();
        picture = $(this).parent().find('img').attr('src');
        
        $('.details-container .testimony-desktop p').html(testimony);
        $('.details-container .testimony-desktop .partner-picture').attr({'src':picture});
        $('.details-container .testimony-desktop .partner-name').html(partnerName);
        $('.details-container .testimony-desktop .partner-position').html(partnerPosition);
     });

    /* END Parceiros Mostrar Depoimento */

    /*** Motion do Header ***/
    currentIcon = 1;
    setTimeout(function(){
       startIcons();
    }, 1500);
    function startIcons() {
        setInterval(function() {
            $('.icon-motion img').attr({'src':'image/icon_motion_0' + currentIcon + '.svg'});
            currentIcon++;
            if (currentIcon > 5 ) { currentIcon = 1; }
        }, 800); 
    }       
    /*** END Motion do Header ***/

    /*** Menu Mobile show/hide ***/
    $(function(){
        if ( isMobile() ) {
            var lastScrollTop = 0, delta = 15;
            $(window).scroll(function(){
                var nowScrollTop = $(this).scrollTop();
                if(Math.abs(lastScrollTop - nowScrollTop) >= delta){
                    if (nowScrollTop > lastScrollTop){                       
                        $('header').css({ "height" : "59px" });                 
                        $('header nav ul').removeClass("animate__fadeInDown");                 
                        $('header nav ul').addClass("animate__animated animate__fadeOutUp");
                    } else {
                        $('header').css({ "height" : "99px" }); 
                        $('header nav ul').removeClass("animate__fadeOutUp");                   
                        $('header nav ul').addClass("animate__animated animate__fadeInDown");
                    }
                    lastScrollTop = nowScrollTop;
                }
            });
        }
     });
    /*** END Menu Mobile show/hide ***/

    /*** Form Handler and Validation ***/
    var isEmail = function(email) {
        var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }

    $('#nome').focus(function(){ $(this).removeClass('invalid'); });     
    $('#nome').blur(function(){       
        if($(this).val() == "") {
            $(this).removeClass('valid');
            $(this).addClass('invalid');
        }
        else {
            $(this).removeClass('invalid');
            $(this).addClass('valid');
        }
    });

    $('#email').focus(function(){ $(this).removeClass('invalid'); });
    $('#email').blur(function(){
        if ( !isEmail( $(this).val() ) ) {          
            $(this).removeClass('valid');
            $(this).addClass('invalid');
        }
        else {
            $(this).removeClass('invalid');
            $(this).addClass('valid');
        }
    });

    $('#mensagem').focus(function(){ $(this).removeClass('invalid'); });
    $('#mensagem').blur(function(){
        if($(this).val() == "") {
            $(this).removeClass('valid');
            $(this).addClass('invalid');
        }
        else {
            $(this).removeClass('invalid');
            $(this).addClass('valid');
        }
    });
    
    $('form').submit(function(e){
        error = 0;
        e.preventDefault();

        if( $("#nome").val() == "" ) {
            error = 1;
            $("#nome").addClass('invalid');
        }
        if( $("#email").val() == "" ) {
            error = 1;
            $("#email").addClass('invalid');
        }
        if( $("#mensagem").val() == "" ) {
            error = 1;
            $("#mensagem").addClass('invalid');
        }

        if ( error == 0 ) {
            showSending();
            $.post( "form.php", $( "form" ).serialize(), 
                function ( data ) { 
                    if (data.error == 0) {
                        showThanks();                        
                    } 
                    else {
                        console.log(data.errorMsg);
                    }
                }
            );               
        }
    });
    $(".closeResponse").click( function(e) { 
        e.preventDefault();
        closeResponse();
    });

    function showSending () {
        $(".formResponse").fadeIn(400).css({ "display" : "flex" });
        $(".sending").delay(200).fadeIn(400);
    } 
    function showThanks () {
        console.log("show");
        $(".sending").hide();
        $(".thanks").show();
        $(".btn-viewport").css({ "display" : "flex" });
        $("#nome, #email, #mensagem").val("").removeClass("valid");
        setTimeout( function() { closeResponse(); }, 5000);
    }
    function closeResponse () {
        console.log("close");
        $(".formResponse").fadeOut( function () {
            $(".sending").hide();
            $(".thanks").hide();
            $(".btn-viewport").hide();
        });
    }
    /*** Form Handler and Validation ***/
});