<?php
// Tratamento de Assunto / Destinatários
if ( $_POST['assunto'] == "email_geral" ) { 
	$subject = "Formulário Veeet (Hot-site Tutores) - Contato";
	$recipients = array('vargas.flavio@gmail.com');
}
if ( $_POST['assunto'] == "email_sugestao" ) { 
	$subject = "Formulário Veeet (Hot-site Tutores) - Sugestão";
	$recipients = array('luiz.fernando.ribeiro@gmail.com');
}
if ( $_POST['assunto'] == "email_duvida" ) { 
	$subject = "Formulário Veeet (Hot-site Tutores) - Dúvida";
	$recipients = array('boldrinronaldo@gmail.com');
}
if ( $_POST['assunto'] == "email_feedback" ) { 
	$subject = "Formulário Veeet (Hot-site Tutores) - Feedback";
	$recipients = array('vargas.flavio@gmail.com');
}
if ( $_POST['assunto'] == "email_reclamacao" ) { 
	$subject = "Formulário Veeet (Hot-site Tutores) - Reclamação";
	$recipients = array('vargas.flavio@gmail.com');
}
if ( $_POST['assunto'] == "email_outros" ) { 
	$subject = "Formulário Veeet (Hot-site Tutores) - Outros";
	$recipients = array('vargas.flavio@gmail.com');
}

// Formatação da Mensagem

$nome = $_POST['nome'];
$email = $_POST['email'];
$assunto = $_POST['assunto'];
$mensagem = $_POST['mensagem'];

// Formato HTML
$msgHTML = "";
$msgHTML .= "<strong>Nome: </strong>" . $nome . "<br>";
$msgHTML .= "<strong>E-mail: </strong>" . $email . "<br><br>";
$msgHTML .= "<strong>Mensagem: </strong><br>" . $mensagem;

// Formato Texto
$msgTXT = "";
$msgTXT .= "Nome: " . $nome . "\r\n";
$msgTXT .= "Email: " . $email . "\r\n\r\n";
$msgTXT .= "Mensagem: \r\n" . $mensagem;

// Setup da Classe PHP Mailer
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';

$mail = new PHPMailer();

$nome = $_POST['nome'];
$email = $_POST['email'];
$assunto = $_POST['assunto'];
$mensagem = $_POST['mensagem'];

$mail = new PHPMailer();
$mail->IsSMTP(); // envia por SMTP 
$mail->CharSet = 'UTF-8';
$mail->Host = "smtp.gmail.com"; // Servidor SMTP
$mail->Port = 587; 
$mail->SMTPAuth = true; // Caso o servidor SMTP precise de autenticação
$mail->Username = "vargas.flavio@gmail.com"; // SMTP username
$mail->Password = "ryzsucdwrbyirrox"; // SMTP password
$mail->From = "vargas.flavio@gmail.com"; // From
$mail->FromName = "Veeet" ; // Nome de quem envia o email

foreach( $recipients as $email ) { $mail->AddAddress($email); }

$mail->WordWrap = 50; // Definir quebra de linha
$mail->IsHTML = true ; // Enviar como HTML
$mail->Subject = $subject; // Assunto
$mail->Body = $msgHTML; //Corpo da mensagem caso seja HTML
$mail->AltBody = $msgTXT; //PlainText, para caso quem receber o email não aceite o corpo HTML

// Envio da Mensagem

if ( !$mail->Send() ) {// Envia o email
  	$response =  array( 
		"error" => 1, 
		"errorMsg" => "Erro no envio da mensagem."
	);
} 
else {
	$response =  array( "error" => 0 ); 
}

// Envio de Resposta JSON
header('Content-Type: application/json');
echo json_encode( $response );
?>